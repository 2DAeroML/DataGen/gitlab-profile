# Data Generation

This subgroup contains the projects `nacaFOAM`, `bluffFOAM` and `Interpolation`, which were used to generate the `AirfoilMNIST` dataset and its `bluff body extension`.

## nacaFOAM

`nacaFOAM` is a CFD simulation pipeline using the `OpenFOAM` framework to generate a 2D database for machine learning purposes using NACA symmetrical, 4- and 5-digit airfoils. 
For more details, please refer to [nacaFOAM](https://gitlab.lrz.de/2DAeroML/DataGen/nacafoam).

## bluffFOAM

`bluffFOAM` is a CFD simulation pipeline using the `OpenFOAM` framework to generate a 2D database for machine learning purposes using bluff body shapes generated through a self-made 5-digits coding system. 
For more details, please refer to [bluffFOAM](https://gitlab.lrz.de/2DAeroML/DataGen/bluffFOAM).

## Interpolation

`Interpolation` is a postprocessing tool that was used to generate the databases based on the raw simulation output.
For more details, please refer to [Interpolation](https://gitlab.lrz.de/2DAeroML/DataGen/Interpolation).